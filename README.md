The following mini project analyzes data in file data.json
What it does is it splits the lines one by one into an array.
Then feeds the data ( text field and filtered field ) into
Token Vectorizer then TfId transformer in order to obtain
a training and test datasets.

Then a NaiveBayes classifier is used to train given a subset of data
and then tested for accuracy using another smaller subset of data.

The result is that this classifier can predict with certainty of aprox. 85%
if a piece of text needs to be filtered (1) or non-filtered (0)