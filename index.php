<?php

ini_set('memory_limit', '4096M');

require_once __DIR__ . '/vendor/autoload.php';

use Phpml\FeatureExtraction\TokenCountVectorizer;
use Phpml\Tokenization\WordTokenizer;
use Phpml\FeatureExtraction\TfIdfTransformer;
use Phpml\CrossValidation\StratifiedRandomSplit;
use Phpml\Dataset\ArrayDataset;
use Phpml\Dataset\CsvDataset;
use Phpml\Metric\Accuracy;
use Phpml\Classification\NaiveBayes;

/*
 *
 *  The following script analyzes data in file data.json
 *  What it does is it splits the lines one by one into an array.
 *  Then feeds the data ( text field and filtered field ) into
 *  Token Vectorizer then TfId transformer in order to obtain
 *  a training and test datasets.
 *
 *  Then a NaiveBayes classifier is used to train given a subset of data
 *  and then tested for accuracy using another smaller subset of data.
 *
 *  The result is that this classifier can predict with certainty of aprox. 85%
 *  if a piece of text needs to be filtered (1) or non-filtered (0)
 *
 */

try{

    // Step 0: Lets read data.json line by line and put text field and filtered filed into an array.
    // Then lets dump that into a csv file

    $lines = file('data.json');

    $samples = [];

    $fp = fopen('file.csv', 'w');

    foreach ($lines as $line){
        $json = json_decode($line);
        $samples[] = array($json->text, $json->filtered);
    }

    //echo 'total lines is ' . count($lines) . PHP_EOL;

    foreach ($samples as $fields) {
        fputcsv($fp, $fields);
    }

    rewind($fp);
    fclose($fp);


    // Step 1: Lets create the Dataset

    $dataset = new CsvDataset('file.csv',1, false);

    echo '$dataset count is ' . count($dataset->getSamples()) . PHP_EOL;

    $samples_text = [];
    foreach ($dataset->getSamples() as $sample) {
        $samples_text[] = $sample[0];
        //echo $sample[0] . PHP_EOL;
    }

    echo '$samples_text count is ' . count($samples_text) . PHP_EOL;

    // Step 2: Lets prepare our Dataset
    // We will use vectorizer (parse the text into a collection of tokens)
    // We will use a Tf-idf Transformer (document frequency)

    $vectorizer = new TokenCountVectorizer(new WordTokenizer());
    $tfIdfTransformer = new TfIdfTransformer();

    $vectorizer->fit($samples_text);
    $vectorizer->transform($samples_text);

    $tfIdfTransformer->fit($samples_text);
    $tfIdfTransformer->transform($samples_text);


    // Step 3: Let's Generate the Training Dataset

    $dataset = new ArrayDataset($samples_text, $dataset->getTargets());

    // Lets split our dataset randomly, where part of data will be used as training data and test data

    $randomSplit = new StratifiedRandomSplit($dataset, 0.1);

    $trainingSamples = $randomSplit->getTrainSamples();
    $trainingLabels     = $randomSplit->getTrainLabels();

    echo '$trainingSamples count is ' . count($trainingSamples) . PHP_EOL;
    echo '$trainingLabels count is ' . count($trainingLabels) . PHP_EOL;

    $testSamples = $randomSplit->getTestSamples();
    $testLabels      = $randomSplit->getTestLabels();

    echo '$testSamples count is ' . count($testSamples) . PHP_EOL;
    echo '$testLabels count is ' . count($testLabels) . PHP_EOL;

    // Step 4: Train the Classifier
    // We will use NaiveBayes classifier

    $classifier = new NaiveBayes();

    echo 'Training classifier...' . PHP_EOL;

    $classifier->train($testSamples, $testLabels);

    echo 'Classifier running predictions...' . PHP_EOL;

    $predictedLabels =  $classifier->predict($testSamples);

    echo '$predictedLabels count is ' . count($predictedLabels) . PHP_EOL;

    // Step 5: Lets verify the Classifier’s Accuracy

    echo 'Accuracy: ' . Accuracy::score($testLabels, $predictedLabels) . PHP_EOL;

    echo PHP_EOL . 'DONE...' . PHP_EOL;
}
catch (\Exception $ex){
    echo $ex->getMessage() . PHP_EOL;
}




